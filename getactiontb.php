<?php 

// This is just an example of reading server side data and sending it to the client.
// It reads a json formatted text file and outputs it.
if(isset($_POST['param1']))
{
    $param1 = $_POST['param1'];

    // Do whatever you want with the $uid
}


// Instead you can query your database and parse into JSON etc etc
$server= 'localhost';
$username = 'root';
$password ='';
$database = 'test';
$maxcols = 6; 

$conn = mysqli_connect($server, $username, $password,$database);

if(!$conn){
    die("Connection Failed :" . mysqli_connect_error());
}



//$sql = "select week_no,less_50 from metrics where emp_id=1 order by week_no desc";
$sql = "select e.emp_id,e.emp_name,p.metric_name,p.value,p.ac_plan,p.week_no,p.additional_comments from employee e join actionplan p where p.empid = e.emp_id and emp_id = '1' and metric_name='Effective Utilization'";

$result = mysqli_query($conn,$sql);


$htmldata ='';
$htmldata .= '<table border="1">';
while($row = mysqli_fetch_row($result)){
    $htmldata .='<tr>';
    for($x = 0 ;$x < 5 ; $x++){
                $htmldata .= '<td>'.$row[$x].'</td>';
    }
    $htmldata .= "</tr>";
    
}
$htmldata .='</table>';

echo $htmldata;
?>
