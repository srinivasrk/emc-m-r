# EMC M & R (Monitoring & Reporting) #

### Overview ###
This Web-application will provide monitoring on various aspects of EMC daily work, such as , Time card Compliance, Utilization, SABA Skills etc., This also provides interactive charts and tables for ease of use. Using HTML, CSS, JS and JQUERY for front end and back end being MySQL this fully fledged application is being used by multiple teams now.

### Interface ###
* ![page 3.png](https://bitbucket.org/repo/AKnaXj/images/742514555-page%203.png)

* ![page 2.PNG](https://bitbucket.org/repo/AKnaXj/images/902820042-page%202.PNG)

* ![page1.png](https://bitbucket.org/repo/AKnaXj/images/2435209576-page1.png)