<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title> EMC M & R </title>

        <!-- Adding JQuery - Inevitabily :( -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
          <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <script type="text/javascript" src="js/creative.js"> </script>
        
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/business-casual.css" rel="stylesheet">

         <!-- Plugin CSS -->
        <link rel="stylesheet" href="css/animate.min.css" type="text/css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/creative.css" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <!-- Fonts -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
         <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      
    </script>
    </head>
    <body>
        <div class="header">
            <img style="width:100%;" src ="img/EMC_proven_professional_top_banner.jpg" alt="EMC Logo"/>
        </div>
        <div id="nav">
            <ul style="list-style-type:none">
                <li class="list-nav-item" style="margin-top : 20px;padding-right: 20px;"><a href="index.php"><img align="center" style="width:100%;height:120px;" src="img/dashboard-icon.png" /> </a></li>
                <li class="list-nav-item" style="margin-top : 40px;padding-right : 20px"><a href="index-metric.php"><img style="width:100%;height:100px;" src="img/metrics.png" /></a> </li>
                <li class="list-nav-item" style="margin-top : 40px;padding-right : 20px"><a href="index-manager.php"><img style="width:110%;height:120px;" src="img/employee-icon.png" /></a> </li>
                 <li class="list-nav-item" style="margin-top : 40px;padding-right : 20px"><a href="escalation-mgr.php"><img style="width:110%;height:120px;" src="img/escalation.jpg" /></a> </li>
            </ul>
        </div>
        <div id="content" >
            <div class="container">
                <div class="BodyBanner">
                    <p class="BodyBannerText"> Manager Metrics Dashboard</p>
                </div>
                <hr />
                <div id="loading" style="display:none;opacity:.75">
                    <div align="center"> <img style="margin-left:250px;margin-top:100px" id="loading-image" src="img/loading.jpg" alt="Loading..." /> </div>
                </div>
                
              <p id="qtr_val" style="font-size:14px;font-weight:600;color:gray;float:left;padding-left:10px;margin-left:10px;">  </p>
                
                <div class="dropdown" align="center" style="margin-top:60px;">
                    <button id="manager_sel" class="btn btn-primary dropdown-toggle" style="background-color:darkturquoise;" type="button" data-toggle="dropdown">Selected Manager
                    <span class="caret"></span><p id="mgr-curr" style="font-weight:500;padding-left:10px; font-size : 14px;display:inline;color:white"> EMP NAME</p></button> 
                  
                        <ul id="results" class="dropdown-menu" style="width: 100%; overflow: hidden;position: relative; float: left; ">
                          <?php
                            $server= 'localhost';
                            $username = 'root';
                            $password ='';
                            $database = 'test';
                            $conn = mysqli_connect($server, $username, $password,$database);
                            
                            if(!$conn){
                                die("Connection Failed :" . mysqli_connect_error());
                            }
                            
                            
                            $sql = "select * from employee where emp_id in (select empid from mapping where mgrid = '136460')";
                            $result = mysqli_query($conn, $sql);

  
                            while ($row=mysqli_fetch_row($result))
                            {
                                 echo '<li align="center" class="mgr-name"><a href="#">'.$row[1].'</a></li>';
                            }
                            
                            ?>
                        <!--  <li align="center"><a href="#">HTML</a></li>
                          <li align="center"><a href="#">CSS</a></li>
                          <li align="center"><a href="#">JavaScript</a></li>-->
                        </ul>
                   
                </div>
                
            <table id="mgr_week_view" class="container table table-bordered" style="padding:20px;margin:20px;">
               
            </table>
                
                <br /><br /><br />
                   <div class="row" style="width : 100%; height:150px;">
                    <div class="col-lg-12" style="padding : 20px;">
                            <div class="checkbox " style="padding-left: 20px; margin : 10px" align="center">
                                <label  style="padding-left: 50px;font-weight:900;"><input id="checkbox1" type="checkbox"  name="tc_complaince" value="">Time Card Complaince</label>
                                <label style="padding-left: 50px;font-weight:900;"><input id="checkbox2" type="checkbox"  name="eff_util" value="">Effective Utilization</label>
                                <label style="padding-left: 50px;font-weight:900;"><input id="checkbox3" type="checkbox" name="bill_util" value="">Billable Utilization</label>
                                <label style="padding-left: 50px;font-weight:900;"><input id="checkbox4" type="checkbox" name="less_50"value="">Members Less than 50% Utilized</label>
                            </div>
                            <div class="checkbox" style="padding-left : 20px; margin : 10px" align="center">
                                <label style="padding-left: 50px;font-weight:900;"><input id="checkbox5" type="checkbox" name="usd_rejects"  value="">USD Rejections</label>
                                <label style="padding-left: 50px;font-weight:900;"><input id="checkbox6" type="checkbox" name="effective_utilisation_eoq" value="">Forecast - Effective Utilization for EOQ</label>
                                <label style="padding-left: 50px;font-weight:900;"><input id="checkbox7" type="checkbox" name="qnbr_complaince" value="">QNBR Complaince</label>
                                <label style="padding-left: 50px;font-weight:900;"><input id="checkbox8" type="checkbox" name="saba_compliant" value="">SABA Complaince</label>
                            </div>
                            <div class="checkbox" style="padding-left : 20px; margin : 10px" align="center">
                                <label style="padding-left: 50px;font-weight:900;"><input id="checkbox9" type="checkbox" name="resume_upload"  value="">Resume Upload</label>
                                <label style="padding-left: 50px;font-weight:900;"><input id="checkbox10" type="checkbox" name="escalations" value="">Escalations</label>
                                <label style="padding-left: 50px;font-weight:900;"><input id="checkbox11" type="checkbox" name="gsap_plan" value="">GSAP Plan</label>
                                <label style="padding-left: 50px;font-weight:900;"><input id="checkbox12" type="checkbox" name="attrition_rate" value="">Attrition</label>
                            </div>
                           
                            <div class="checkbox" style="padding-left : 20px; margin : 10px" align="center" >
                                <label style="padding-left: 50px;font-weight:900;"><input id="checkbox13" type="checkbox" name="inter_div_mov"  value="">Short/Long Term Interdivision Movement</label>
                                <label style="padding-left: 50px;font-weight:900;"><input id="checkbox14" type="checkbox" name="onsite_sl" value="">On-Site Project - Short/long term</label>
                                
                            </div>
                    </div>
                </div>
                 <div id="loading_reason" style="display:none;padding:50px;">
                    <div style="margin:50px;" >
                    <a class="close" style="padding:10px" onclick="closepopup1()">×</a>
                    <div id="reason_data">
                    
                    </div>
                    </div>
                </div>
                <div id="curve_chart" style="width: 900px; height: 500px;"></div>

            </div>
        <footer class="footer">
            <hr />
            <p align="center" class="BodyBannerText" style="font-size:14px;color:red;" > All Rights Reserved - EMC 2016</p>
        </footer>
        </div>

    </body>
</html>