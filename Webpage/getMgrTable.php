<?php 

function getMetricName($m) {
    switch (trim($m)){
            
        case "ATTR. RATE":
            return "attrition_rate";
            
        case "BILL UTIL":
            return "bill_util";
        
        case "EFF UTIL - FORECAST EOQ":
            return "eff_util";
        
        case "EFF. UTIL":
            return "effective_utilisation_eoq";
            
        case "ESCL.":
            return "escalations";
            
        case "GSAP plan";
            return "gsap_plan";
            
        case "INTER DIV MOVE";
            return "inter_div_mov";
            
        case "LESS THAN 50% UTILIZED":
            return "less_50";
        
        case "ONSITE SHORT AND LNG TERM MOV.":
            return "onsite_sl";
            
        case "QNBR COMP.":
            return "qnbr_complaince";
        
        case "RESUME UPLOAD":
            return "resume_upload";
        
        case "SABA COMP.":
            return "saba_compliant";
        
        case "TC COMP.":
            return "tc_complaince";
        
        case "VSD PROJECT REJECTS":
            return "usd_rejects";
    }
}
function isRed ($m){
    switch (trim($m)){
            
        case "escalations":
            return "true";
        case "attrition_rate":
            return "true";
        case "less_50":
            return "true";
        case "usd_rejects":
            return "true";
        default :
            return "false";
    }
}
function isColored($m) {
    
    switch (trim($m)){
            
        case "bill_util":
            return "true";
        
        case "eff_util":
            return "true";
        
        case "effective_utilisation_eoq":
            return "true";
            
        case "qnbr_complaince":
            return "true";
        
        case "resume_upload":
            return "true";
        
        case "saba_compliant":
            return "true";
        
        case "tc_complaince":
            return "true";
            
        default :
            return "false";
        
    }
}
// This is just an example of reading server side data and sending it to the client.
// It reads a json formatted text file and outputs it.
if(isset($_POST['param1']) && isset($_POST['param2']))
{
    $param1 = $_POST['param1'];
    $param2 = $_POST['param2']; // Quarter
   
}


// Instead you can query your database and parse into JSON etc etc
$server= 'localhost';
$username = 'root';
$password ='';
$database = 'test';

$conn = mysqli_connect($server, $username, $password,$database);

if(!$conn){
    die("Connection Failed :" . mysqli_connect_error());
}



//$sql = "select week_no,less_50 from metrics where emp_id=1 order by week_no desc";
$sql = "select * from metrics m join employee e on m.emp_id = e.emp_id and e.emp_name='".$param1."' and quarter ='".$param2."' and week_no >'1' and week_no <'13' order by week_no desc";

$met_data_result = mysqli_query($conn,$sql);

$htmlheaderdata ='<thead>';
$htmlheaderdata .= '<tr>';
$htmlheaderdata .= '<th  style="padding:10px;color:red;text-decoration:underline";>'. $param1 .'</th>';
while($row = mysqli_fetch_row($met_data_result)){
        
        $htmlheaderdata .= '<th style="padding:10px";> Week Number '.$row[1].'</th>';
    }
    $htmlheaderdata .= "</tr>";
    $htmlheaderdata .= "</thead>";
$sql = "select * from metric_names order by metric_name;";

$met_name_reuslt = mysqli_query($conn,$sql);

$htmlrowdata ='<tbody>';



while($met_name_row = mysqli_fetch_row($met_name_reuslt)){
    $htmlrowdata .= '<tr>';
    $htmlrowdata .= '<th style="padding:10px;">'.$met_name_row[0].'</th>';
    $metric_name = getMetricName($met_name_row[0]);
   
    mysqli_data_seek($met_data_result, 0);
    while($met_data_row = mysqli_fetch_row($met_data_result)){
        
        $sql1 = "select * from metrics m join employee e on m.emp_id = e.emp_id and e.emp_name='".$param1."' and week_no='" .$met_data_row[1]."'";
        $result1 = mysqli_query($conn,$sql1);
        $row2 = mysqli_fetch_assoc($result1);
        if(intval($row2[$metric_name]) <70 && (isColored($metric_name) == "true")){
            $htmlrowdata .= '<td style="margin:10px;padding : 10px;font-weight:600;" class="redclass" onclick="getId(this)">'.$row2[$metric_name].'</td>';
        }
        else if(intval($row2[$metric_name]) <=95 && (isColored($metric_name) == "true")){
            $htmlrowdata .= '<td style="margin:10px;padding : 10px;font-weight:600;" class="amberclass" onclick="getId(this)">'.$row2[$metric_name].'</td>';
        }
        else if((isColored($metric_name) == "true")){
            $htmlrowdata .= '<td style="margin:10px;padding : 10px;font-weight:600;" class="greenclass" onclick="getId(this)">'.$row2[$metric_name].'</td>';
        }
        else{
            if(isRed($metric_name)=="true")
            {
                if($row2[$metric_name] > 0)
                {
                    $htmlrowdata .= '<td style="margin:10px;padding : 10px;font-weight:600;" class="redclass" onclick="getId(this)">'.$row2[$metric_name].'</td>';
                }
                else{
                    $htmlrowdata .= '<td style="margin:10px;padding : 10px;font-weight:600;" class="greenclass" onclick="getId(this)">'.$row2[$metric_name].'</td>';
                }
            }
            else{
                $htmlrowdata .= '<td style="margin:10px;padding : 10px;font-weight:600;" class="noclass" onclick="getId(this)">'.$row2[$metric_name].'</td>';
            }
              
        }
    }
  
    $htmlrowdata .= '</tr>';
}
$htmlrowdata .= '</tbody>';


echo $htmlheaderdata.$htmlrowdata;
?>