<?php 


// This is just an example of reading server side data and sending it to the client.
// It reads a json formatted text file and outputs it.
if(isset($_POST['param1']) and isset($_POST['param2']) and isset($_POST['param3']))
{
    $param1 = $_POST['param1'];
    $param2 = $_POST['param2'];
    $param3 = $_POST['param3'];
    // Do whatever you want with the $uid
}


// Instead you can query your database and parse into JSON etc etc
$server= 'localhost';
$username = 'root';
$password ='';
$database = 'test';

$conn = mysqli_connect($server, $username, $password,$database);

if(!$conn){
    die("Connection Failed :" . mysqli_connect_error());
}



//$sql = "select week_no,less_50 from metrics where emp_id=1 order by week_no desc";
$sql = "select m1.emp_id, emp_name, m1.tc_complaince, m1.eff_util, m1.bill_util, m1.less_50, m1.onsite_sl,
m1.usd_rejects, m1.qnbr_complaince, m1.saba_compliant, m1.resume_upload, m1.escalations, m1.gsap_plan,
m1.attrition_rate, m1.inter_div_mov, m1.effective_utilisation_eoq from metrics m1 join ((select * from employee where emp_id in (select empid from mapping where mgrid ='136460')) as myselect) where m1.emp_id = myselect.emp_id and quarter='".$param2."' and year='".$param3."' and week_no='". $param1 ."'";


$result = mysqli_query($conn,$sql);

$htmldata ='';
while($row = mysqli_fetch_row($result)){
    $htmldata .= '<tr>';
    for($x = 0 ;$x <= 15 ; $x++){
        if($x >= 2){
            if(intval($row[$x]) <70 && ($x==2 || $x ==3 || $x ==4 || $x==8 || $x==9 || $x==10 || $x==15))
            {
                $htmldata .= '<td style="font-weight:600;" class="redclass" onclick="getId(this)">'.$row[$x].'</td>';
            }
            else if(intval($row[$x] <=95) && ($x==2 || $x ==3 || $x ==4 || $x==8 || $x==9 || $x==10 || $x==15)){
                $htmldata .= '<td style="font-weight:600;" class="amberclass" onclick="getId(this)">'.$row[$x].'</td>';
            }
             else if(intval(($x==2 || $x ==3 || $x ==4 || $x==8 || $x==9 || $x==10 || $x==15))) {
                $htmldata .= '<td style="font-weight:600;" class="greenclass" onclick="getId(this)">'.$row[$x].'</td>';
            }
            else{
                if($x==5 || $x==7 || $x==11 || $x==13)
                {
                    if(intval($row[$x]) >0)
                    {
                        $htmldata .= '<td style="font-weight:600;" class="redclass"  onclick="getId(this)">'.$row[$x].'</td>';
                    }
                    else{
                        $htmldata .= '<td style="font-weight:600;" class="greenclass"  onclick="getId(this)">'.$row[$x].'</td>';
                    }
                }
                else{
                    $htmldata .= '<td style="font-weight:600;" class="noclass"  onclick="getId(this)">'.$row[$x].'</td>';
                }
            }
        }
        else{
            if($x==0)
            {
                $htmldata .= '<td style="font-weight:600;display:none;">'.$row[$x].'</td>';
            }
            else{
                $htmldata .= '<td style="font-weight:600;">'.$row[$x].'</td>';
            }
        }
    }
    $htmldata .= "</tr>";
}

echo $htmldata;
?>