$(document).ready(function(){
    
   
    
    
    if(document.getElementById("escl_data"))
        {
            $.ajax({
              type:'POST',
              url: "getescl.php",
              data: {},
              dataType: "html",
              cache: false,
              async: false,
              success: function(data){
                if(data){
                    //sleep(5000);
                   $("#escl_data").html(data);

                    //drawChart(array, "My Chart", "Data");
                }
                  else{
                      alert("Error");
                  }

            },
             async: "false",
        });
        
            
        }
    
    
    if(document.getElementById("mgr_week_view"))
    {
        $("#mgr_week_view").html("");
    }
    
    
        Date.prototype.getWeek = function () {
        var target  = new Date(this.valueOf());
        var dayNr   = (this.getDay() + 6) % 7;
        target.setDate(target.getDate() - dayNr + 3);
        var firstThursday = target.valueOf();
        target.setMonth(0, 1);
        if (target.getDay() != 4) {
            target.setMonth(0, 1 + ((4 - target.getDay()) + 7) % 7);
        }
        return 1 + Math.ceil((firstThursday - target) / 604800000);
    }
    var d= new Date();
    var quarter;
    if (d.getMonth() < 4)
      quarter = 1;
    else if (d.getMonth() < 7)
      quarter = 2;
    else if (d.getMonth() < 10)
      quarter = 3;
    else if (d.getMonth() < 13)
      quarter = 4;

    if(document.getElementById("overall_week_view"))
        {
            $.ajax({
              type:'POST',
              url: "getOverallTbl.php",
              data: { param1 : quarter },
              dataType: "html",
              cache: false,
              async: false,
              success: function(data){
                if(data){
                    //sleep(5000);
                   $("#overall_week_view").html(data);

                    //drawChart(array, "My Chart", "Data");
                }
                  else{
                      alert("Error");
                  }

            },
             async: "false",
        });
        
            
        }
    // 1 to 13 - Q1 , 14-26 -Q2 , 27-39 - Q3 , 40 - 53 Q4

  if(document.getElementById("qtr_val"))
      {
        var date = new Date();
        startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
        document.getElementById("qtr_val").innerHTML = "Current Quarter : "+ quarter + " Current Week : " + startDate.getWeek()%13;
      }
    
    if(document.getElementsByTagName("ul")[1]){
    var list = document.getElementsByTagName("ul")[1];
        


    if(document.getElementById("mgr_week_view"))
        {
        $("#mgr-curr").html(list.getElementsByTagName("li")[0].innerText);
        var mgrname = $("#mgr-curr").html();

              $.ajax({
              type:'POST',
              url: "getMgrTable.php",
              data: { param1 : mgrname, param2 : quarter },
              dataType: "html",
              cache: false,
              async: false,
              success: function(data){
                if(data){
                    //sleep(5000);
                   $("#mgr_week_view").html(data);

                    //drawChart(array, "My Chart", "Data");
                }
                  else{
                      alert("Error");
                  }

            },
             async: "false",
        });
        
    }
        
        
}
     var curr_manager='';
      var options = {
          title: 'Performance',
          curveType: 'function',
          legend: { position: 'right' }
        };
    google.charts.load('current', {'packages':['corechart']});
    
    $(":checkbox").click(function(){
    // Set a callback to run when the Google Visualization API is loaded.
  
    var postParameter='';
//set the manager name as current manager to send to PHP
    var mgrname = $("#mgr-curr").html();
    //alert(mgrname);
    for(i=1;i<=14;i++)
        {
            if(document.getElementById('checkbox'+i).checked == true)
                {
                    postParameter = postParameter+document.getElementById('checkbox'+i).name+' | ';
                    
                }
        }
     mgrname = mgrname.trim();
     $.ajax({
          type:'POST',
          url: "getData.php",
          data: { param1 : postParameter, param2 : mgrname },
          dataType: "json",
          cache: false,
          async: false,
          success: function(array){
            if(array){
                //sleep(5000);
                $("#loading").show().delay(3000).fadeOut();
                var data = new google.visualization.DataTable(array);
                var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
                chart.draw(data,options, {width: 500, height: 500});
                
                //drawChart(array, "My Chart", "Data");
            }
              
        },
         async: "false",
    });
     
        

    });
    
     $( "#results" ).on("click","li.mgr-name", function(event) {
          
         $("#mgr-curr").html($(this).text());
          var postParameter='';
//set the manager name as current manager to send to PHP
    var mgrname = $("#mgr-curr").html();
    //alert(mgrname);
    for(i=1;i<=14;i++)
        {
            if(document.getElementById('checkbox'+i).checked == true)
                {
                    postParameter = postParameter+document.getElementById('checkbox'+i).name+' | ';
                    
                }
        }
   
     $.ajax({
          type:'POST',
          url: "getData.php",
          data: { param1 : postParameter, param2 : mgrname },
          dataType: "json",
          cache: false,
          async: false,
          success: function(array){
            if(array){
                //sleep(5000);
                $("#loading").show().delay(3000).fadeOut();
                var data = new google.visualization.DataTable(array);
                var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
                chart.draw(data,options, {width: 500, height: 500});
                
                //drawChart(array, "My Chart", "Data");
            }
              
        },
         async: "false",
    });
        
         $("#mgr_week_view").html("");
          $.ajax({
          type:'POST',
          url: "getMgrTable.php",
          data: { param1 : mgrname, param2 : quarter },
          dataType: "html",
          cache: false,
          async: false,
          success: function(data){
            if(data){
                //sleep(5000);
               $("#mgr_week_view").html(data);
                
                //drawChart(array, "My Chart", "Data");
            }
              else{
                  alert("Error");
              }
              
        },
         async: "false",
    });
         
    });    
   
    
  
});
/*var delay = ( function() {
    var timer = 0;
    return function(callback, ms) {
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
        
    };
})();*/
function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function  getId(element) {
    

var table = document.getElementsByTagName("table")[0];
var cells = table.getElementsByTagName("td"); 


var metric_name = table.rows[element.parentNode.rowIndex].cells[0].innerText;
var mgrname = $("#mgr-curr").html();
var week_no1 =table.rows[0].cells[element.cellIndex].innerText;
   // var metric = table.rows[0].cells[element.cellIndex].innerText;
    var id='';
var res = week_no1.split(" ");
    $.ajax({
        type:'POST',
        url: "getID.php",
        data: {param1 : mgrname},
        cache: false,
        async: false,
        success: function(data){
            id = data;
        }
    });
    
   
   
        $.ajax({
          type:'POST',
          url: "getReason.php",
          data: { param1 : id.trim() , param2 : metric_name.trim() , param3 : res[2]},
          cache: false,
          async: false,
          success: function(data){
            if(data){
                //sleep(5000);
                $("#reason_data").html(data);
                $("#loading_reason").show();
                //drawChart(array, "My Chart", "Data");
            }
              
        },
         async: "false",
    });
        
}


function closepopup1(){
     $("#loading_reason").hide();
}

    function drawChart(char_data,chart_title,chart_title1) {
     
      // Create our data table out of JSON data loaded from server.
        
      
      
  
         
        
      // Instantiate and draw our chart, passing in some options.
     // var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
      //chart.draw(data,options, {width: 500, height: 500});
    }