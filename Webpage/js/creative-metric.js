

function findqtr(param1){
    
    var d= new Date(param1);
    
    var quarter;
    if (d.getMonth()+1 < 4)
      quarter = 1;
    else if (d.getMonth()+1 < 7)
      quarter = 2;
    else if (d.getMonth()+1 < 10)
      quarter = 3;
    else if (d.getMonth()+1 < 13)
      quarter = 4;
    
    return quarter;
}

function getProperWeekNumber(param1){
    var curr_date = new Date(param1);
    var d = new Date(param1);
    while(d.getMonth()+1 != 1 && d.getMonth()+1 !=4 && d.getMonth()+1 !=7 && d.getMonth()+1 != 10)
        {
           d.setDate(d.getDate() - 7);
        }
    if(d.getDate() > 7)
    {
        while(d.getDate() != 7 ){
            d.setDate(d.getDate()-1);
        }
    }
    var temp = d.getDate();
    while(temp!=0)
        {
            if(d.getDay() ==0){
                startdate = d;
                break;
            }
            else{
                d.setDate(d.getDate()-1);
            }
            temp --;
        }
   
    var firstDate = new Date(startdate.getYear(),startdate.getMonth()+1,startdate.getDate());
    var secondDate = new Date(curr_date.getYear(),curr_date.getMonth()+1,curr_date.getDate());
  
    var diffdate = Math.round(Math.abs((firstDate.getTime()-secondDate.getTime())/(24*60*60*1000)));
  
    if(diffdate <= 90){
    return(Math.round(diffdate/7)+1);
    }
    else{
        return 14;
    }
   
}

 var week_no = 0;
$(document).ready(function(){
     
 // Set a callback to run when the Google Visualization API is loaded.
    var postParameter='';

    
    Date.prototype.getWeek = function () {
    var curr_date = new Date(+this);
    var d = new Date(+this);
        while(d.getMonth()+1 != 1 && d.getMonth()+1 !=4 && d.getMonth()+1 !=7 && d.getMonth()+1 != 10)
        {
           d.setDate(d.getDate() - 7);
        }
    if(d.getDate() > 7)
    {
        while(d.getDate() != 7 ){
            d.setDate(d.getDate()-1);
        }
    }
    var temp = d.getDate();
    while(temp!=0)
        {
            if(d.getDay() ==0){
                startdate = d;
                break;
            }
            else{
                d.setDate(d.getDate()-1);
            }
            temp --;
        }
   
    var firstDate = new Date(startdate.getYear(),startdate.getMonth()+1,startdate.getDate());
    var secondDate = new Date(curr_date.getYear(),curr_date.getMonth()+1,curr_date.getDate());
  
    var diffdate = Math.round(Math.abs((firstDate.getTime()-secondDate.getTime())/(24*60*60*1000)));
  
         if(diffdate <= 90){
        postParameter = (Math.round(diffdate/7)+1);
    }
    else{
        postParameter = 14;
    }
    postParameter = (Math.round(diffdate/7)+1);
        
 
        postParameter = (Math.round(diffdate/7)+1);
        var qtr_val = findqtr(curr_date);
        var year = curr_date.getFullYear();
        
        
        // need to pass quarter as well with parameter
    $("tbody").html("");
        $.ajax({
          type:'POST',
          url: "getTable.php",
          data: { param1 : postParameter, param2 : qtr_val, param3 : year },
          dataType: "html",
          cache: false,
          async: false,
          success: function(data){
            if(data){
                //sleep(5000);
                
                 $("tbody").html(data);
                
                //drawChart(array, "My Chart", "Data");
            }
              
        },
         async: "false",
    });
        
   if(diffdate <= 90){
    return(Math.round(diffdate/7)+1);
    }
    else{
        return 14;
    }
   
   }
    
  
        
   
   $(function() {
    var startDate;
    var endDate;
    
    var selectCurrentWeek = function() {
        window.setTimeout(function () {
            $('#weekpicker').datepicker('widget').find('.ui-datepicker-current-day a').addClass('ui-state-active')
        }, 1);
    }
    
    $('#weekpicker').datepicker( {
        showOtherMonths: false,
        selectOtherMonths: false,
        onSelect: function(dateText, inst) { 
            var date = $(this).datepicker('getDate');
            startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
          
           week_no = startDate.getWeek();

            document.getElementById("qtr_val").innerHTML = "Current Quarter "+ findqtr(startDate)  +" Week number : " + week_no;
                       
            endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
            var dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
            $('#weekpicker').val($.datepicker.formatDate( dateFormat, startDate, inst.settings )
                 + ' - ' + $.datepicker.formatDate( dateFormat, endDate, inst.settings ));
            
            selectCurrentWeek();
        },
        beforeShow: function() {
            selectCurrentWeek();
        },
        beforeShowDay: function(date) {
            var cssClass = '';
            if(date >= startDate && date <= endDate)
                cssClass = 'ui-datepicker-current-day';
            return [true, cssClass];
        },
        onChangeMonthYear: function(year, month, inst) {
            selectCurrentWeek();
        }
    }).datepicker('widget').addClass('ui-weekpicker');
    
    $('.ui-weekpicker .ui-datepicker-calendar tr').live('mousemove', function() { $(this).find('td a').addClass('ui-state-hover'); });
    $('.ui-weekpicker .ui-datepicker-calendar tr').live('mouseleave', function() { $(this).find('td a').removeClass('ui-state-hover'); });
   
});
    
    $('#weekpicker').datepicker('setDate',new Date());  
    $('.ui-datepicker-current-day').click();
  
     
     
    
});
/*var delay = ( function() {
    var timer = 0;
    return function(callback, ms) {
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
        
    };
})();*/
function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}


function  getId(element) {
    

var table = document.getElementsByTagName("table")[0];
var cells = table.getElementsByTagName("td"); 


var id = table.rows[element.parentNode.rowIndex].cells[0].innerText;

var metric =table.rows[0].cells[element.cellIndex].innerText;
   // var metric = table.rows[0].cells[element.cellIndex].innerText;

        $.ajax({
          type:'POST',
          url: "getReason.php",
          data: { param1 : id.trim() , param2 : metric.trim() , param3 : week_no},
          cache: false,
          async: false,
          success: function(data){
            if(data){
                //sleep(5000);
                $("#reason_data").html(data);
                $("#loading").show();
                //drawChart(array, "My Chart", "Data");
            }
              
        },
         async: "false",
    });
        
}


function closepopup(){
     $("#loading").hide();
}

 
    
