<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title> EMC M & R </title>

          <!-- Adding JQuery - Inevitabily :( -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
         <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
        
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
          <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/base/jquery-ui.css">
        
        <script type="text/javascript" src="js/creative-metric.js"></script>
      
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
      
        <!-- Custom CSS -->
        <link href="css/business-casual.css" rel="stylesheet">

         <!-- Plugin CSS -->
        <link rel="stylesheet" href="css/animate.min.css" type="text/css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/creative.css" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        
        <!-- Fonts -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
         
    </head>
    <body>
        <div class="header">
            <img style="width:100%;" src ="img/EMC_proven_professional_top_banner.jpg" alt="EMC Logo"/>
        </div>
        <div id="nav">
            <ul style="list-style-type:none">
                <li class="list-nav-item" style="margin-top : 20px;padding-right: 20px;"><a href="index.php"><img align="center" style="width:100%;height:120px;" src="img/dashboard-icon.png" /> </a></li>
                <li class="list-nav-item" style="margin-top : 40px;padding-right : 20px"><a href="index-metric.php"><img style="width:100%;height:100px;" src="img/metrics.png" /></a> </li>
                <li class="list-nav-item" style="margin-top : 40px;padding-right : 20px"><a href="index-manager.php"><img style="width:110%;height:120px;" src="img/employee-icon.png" /></a> </li>
                 <li class="list-nav-item" style="margin-top : 40px;padding-right : 20px"><a href="escalation-mgr.php"><img style="width:110%;height:120px;" src="img/escalation.jpg" /></a> </li>
            </ul>
        </div>
        <div id="content" >
            <div class="container">
                <div class="BodyBanner">
                    <p class="BodyBannerText"> Metrics Dashboard</p>
                </div>
                
                <hr />
              
                <div style="float:left">
                    <p>Select Week : <input type="text" id="weekpicker" required="required" name="newsdate" style="font-size:16px;font-weight:600;width:250px" /></p>
                </div>
                <p id="qtr_val" style="font-size:14px;font-weight:600;color:gray;float:left;padding-left:10px;margin-left:10px;margin-top:10px;">  </p>

    <table class="container table table-bordered" style="padding:20px;">
	<thead>
		<tr>
			<th style="padding:10px; display:none;"><h1>Manager ID</h1></th>
			<th style="padding:10px;"><h1>Manager Name</h1></th>
			<th style="padding:10px;"><h1>TC Comp.</h1></th>
            <th style="padding:10px;"><h1>Eff. Util</h1></th>
            <th style="padding:10px;"><h1>Bill Util</h1></th>
            <th style="padding:10px;"><h1>Less than 50% Utilized</h1></th>
            <th style="padding:10px;"><h1>Onsite Short and Long term mov.</h1></th>
            <th style="padding:10px;"><h1>VSD project rejects</h1></th>
            <th style="padding:10px;"><h1>QNBR Comp.</h1></th>
            <th style="padding:10px;"><h1>SABA Comp.</h1></th>
            <th style="padding:10px;"><h1>Resume Upload</h1></th>
            <th style="padding:10px;"><h1>Escl.</h1></th>
            <th style="padding:10px;"><h1>GSAP plan</h1></th>
            <th style="padding:10px;"><h1>Attr. Rate</h1></th>
            <th style="padding:10px;"><h1>Inter Div Move</h1></th>
            <th style="padding:10px;"><h1>Eff Util - Forecast EOQ</h1></th>
		</tr>
	</thead>
	<tbody class="demo">
		
	</tbody>
</table>
                
                  <div id="loading" style="display:none;padding:50px;">
                    <div style="margin:50px;" >
                    <a class="close" style="padding:10px" onclick="closepopup()">×</a>
                    <div id="reason_data">
                    
                    </div>
                    </div>
                    
                </div>
                
                <footer class="footer">
                <hr />
                <p align="center" class="BodyBannerText" style="font-size:14px;color:red;" > All Rights Reserved - EMC 2016</p>
            </footer>
            </div>
            
        </div>

    </body>
</html>