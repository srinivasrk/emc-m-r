-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.24 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for test
CREATE DATABASE IF NOT EXISTS `test` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `test`;


-- Dumping structure for table test.actionplan
CREATE TABLE IF NOT EXISTS `actionplan` (
  `empid` int(11) DEFAULT NULL,
  `metric_name` varchar(75) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `ac_plan` varchar(500) DEFAULT NULL,
  `week_no` varchar(50) DEFAULT NULL,
  `additional_comments` varchar(500) DEFAULT NULL,
  KEY `FK_actionplan_employee` (`empid`),
  KEY `FK_actionplan_metric_names` (`metric_name`),
  CONSTRAINT `FK_actionplan_employee` FOREIGN KEY (`empid`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_actionplan_metric_names` FOREIGN KEY (`metric_name`) REFERENCES `metric_names` (`metric_name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table test.actionplan: ~0 rows (approximately)
/*!40000 ALTER TABLE `actionplan` DISABLE KEYS */;
/*!40000 ALTER TABLE `actionplan` ENABLE KEYS */;


-- Dumping structure for table test.employee
CREATE TABLE IF NOT EXISTS `employee` (
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `emp_desg` varchar(50) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `last_login` date DEFAULT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table test.employee: ~9 rows (approximately)
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
REPLACE INTO `employee` (`emp_id`, `emp_name`, `emp_desg`, `password`, `last_login`) VALUES
	(83002, 'Mohit', 'q', '123', '2016-09-04'),
	(102141, 'Kirti', 'q', '123', '2016-08-31'),
	(117951, 'Mukesh', 'q', '123', '2016-09-04'),
	(119303, 'Clifford', 'q', '123', '2016-09-04'),
	(122224, 'Venakt', 'q', '123', '2016-09-04'),
	(125297, 'Kamesh', 'q', '123', '2016-09-04'),
	(131399, 'Sandip', 'q', '123', '2016-08-31'),
	(136460, 'Jay', 'q', '123', '2016-09-08'),
	(146153, 'Yatin', 'q', '123', '2016-09-04');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;


-- Dumping structure for table test.mapping
CREATE TABLE IF NOT EXISTS `mapping` (
  `empid` int(11) NOT NULL,
  `mgrid` int(11) DEFAULT NULL,
  KEY `FK_mapping_employee` (`empid`),
  KEY `FK_mapping_employee_2` (`mgrid`),
  CONSTRAINT `FK_mapping_employee` FOREIGN KEY (`empid`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_mapping_employee_2` FOREIGN KEY (`mgrid`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table test.mapping: ~9 rows (approximately)
/*!40000 ALTER TABLE `mapping` DISABLE KEYS */;
REPLACE INTO `mapping` (`empid`, `mgrid`) VALUES
	(119303, 136460),
	(125297, 136460),
	(102141, 136460),
	(83002, 136460),
	(117951, 136460),
	(131399, 136460),
	(122224, 136460),
	(146153, 136460),
	(136460, 136460);
/*!40000 ALTER TABLE `mapping` ENABLE KEYS */;


-- Dumping structure for table test.metrics
CREATE TABLE IF NOT EXISTS `metrics` (
  `emp_id` int(11) DEFAULT NULL,
  `week_no` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `tc_complaince` int(11) NOT NULL,
  `eff_util` int(11) NOT NULL,
  `bill_util` int(11) NOT NULL,
  `less_50` int(11) NOT NULL,
  `onsite_sl` int(11) NOT NULL,
  `usd_rejects` int(11) NOT NULL,
  `qnbr_complaince` int(11) NOT NULL,
  `saba_compliant` int(11) NOT NULL,
  `resume_upload` int(11) NOT NULL,
  `escalations` int(11) NOT NULL,
  `gsap_plan` int(11) NOT NULL,
  `attrition_rate` int(11) NOT NULL,
  `inter_div_mov` int(11) NOT NULL,
  `effective_utilisation_eoq` int(11) NOT NULL,
  KEY `metrics_ibfk_1` (`emp_id`),
  CONSTRAINT `metrics_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table test.metrics: ~45 rows (approximately)
/*!40000 ALTER TABLE `metrics` DISABLE KEYS */;
REPLACE INTO `metrics` (`emp_id`, `week_no`, `year`, `tc_complaince`, `eff_util`, `bill_util`, `less_50`, `onsite_sl`, `usd_rejects`, `qnbr_complaince`, `saba_compliant`, `resume_upload`, `escalations`, `gsap_plan`, `attrition_rate`, `inter_div_mov`, `effective_utilisation_eoq`) VALUES
	(119303, 9, 2016, 100, 65, 59, 2, 0, 0, 100, 100, 100, 0, 0, 0, 0, 78),
	(119303, 8, 2016, 99, 65, 60, 2, 0, 0, 100, 100, 100, 0, 0, 0, 0, 78),
	(119303, 7, 2016, 100, 62, 57, 2, 0, 0, 100, 100, 100, 0, 0, 0, 0, 78),
	(119303, 6, 2016, 100, 60, 57, 2, 0, 0, 100, 100, 100, 0, 0, 0, 0, 78),
	(119303, 5, 2016, 100, 60, 57, 2, 0, 0, 100, 100, 100, 0, 0, 0, 0, 78),
	(125297, 8, 2016, 100, 97, 83, 1, 0, 0, 100, 100, 100, 0, 0, 0, 0, 95),
	(125297, 7, 2016, 100, 96, 84, 1, 0, 0, 100, 100, 100, 0, 0, 0, 0, 95),
	(125297, 6, 2016, 100, 91, 84, 1, 0, 0, 100, 100, 100, 0, 0, 0, 0, 95),
	(125297, 5, 2016, 100, 90, 84, 1, 0, 0, 100, 100, 100, 0, 0, 0, 0, 95),
	(125297, 9, 2016, 100, 99, 86, 1, 0, 0, 100, 100, 100, 0, 0, 0, 0, 95),
	(102141, 9, 2016, 96, 74, 73, 1, 4, 0, 100, 100, 100, 0, 8, 1, 0, 78),
	(102141, 8, 2016, 90, 79, 78, 1, 4, 0, 100, 100, 100, 0, 8, 1, 0, 78),
	(102141, 7, 2016, 100, 99, 99, 1, 4, 0, 100, 100, 100, 0, 8, 1, 0, 78),
	(102141, 6, 2016, 45, 92, 92, 1, 4, 0, 100, 100, 100, 0, 8, 1, 0, 78),
	(83002, 9, 2016, 100, 86, 78, 1, 0, 0, 100, 100, 100, 0, 5, 2, 0, 84),
	(102141, 5, 2016, 45, 90, 90, 1, 4, 0, 100, 100, 100, 0, 8, 1, 0, 78),
	(83002, 8, 2016, 100, 88, 82, 1, 0, 0, 100, 100, 90, 0, 5, 2, 0, 84),
	(83002, 7, 2016, 100, 119, 106, 1, 0, 0, 100, 100, 90, 0, 5, 2, 0, 84),
	(83002, 6, 2016, 94, 124, 119, 1, 0, 0, 100, 100, 90, 0, 5, 2, 0, 84),
	(83002, 5, 2016, 100, 143, 116, 1, 0, 0, 100, 100, 90, 0, 5, 2, 0, 84),
	(117951, 9, 2016, 97, 78, 74, 2, 0, 0, 100, 100, 100, 0, 0, 0, 0, 76),
	(117951, 8, 2016, 87, 74, 74, 2, 0, 0, 100, 100, 100, 0, 0, 0, 0, 76),
	(117951, 7, 2016, 93, 79, 78, 2, 0, 0, 100, 100, 100, 0, 0, 0, 0, 76),
	(117951, 6, 2016, 93, 79, 78, 2, 0, 0, 100, 100, 100, 1, 0, 0, 0, 76),
	(117951, 5, 2016, 100, 65, 60, 2, 0, 0, 100, 100, 100, 0, 0, 0, 0, 76),
	(131399, 8, 2016, 92, 59, 59, 4, 3, 0, 100, 100, 100, 1, 0, 0, 0, 75),
	(131399, 7, 2016, 92, 59, 66, 4, 3, 0, 100, 100, 100, 0, 0, 0, 0, 75),
	(131399, 6, 2016, 85, 69, 66, 4, 3, 0, 100, 100, 100, 0, 0, 0, 0, 75),
	(131399, 5, 2016, 100, 66, 67, 4, 3, 0, 100, 100, 100, 0, 0, 0, 0, 75),
	(131399, 9, 2016, 92, 62, 61, 4, 3, 0, 100, 100, 100, 0, 0, 0, 0, 75),
	(122224, 9, 2016, 96, 76, 70, 0, 1, 0, 100, 100, 100, 0, 8, 1, 0, 76),
	(122224, 8, 2016, 96, 80, 76, 0, 1, 0, 100, 100, 100, 0, 8, 1, 0, 76),
	(122224, 7, 2016, 96, 79, 77, 0, 1, 0, 100, 100, 100, 0, 8, 1, 0, 76),
	(122224, 6, 2016, 93, 71, 65, 0, 1, 0, 100, 100, 100, 0, 8, 1, 0, 76),
	(122224, 5, 2016, 92, 78, 75, 0, 1, 0, 100, 100, 100, 0, 8, 1, 0, 76),
	(146153, 9, 2016, 100, 85, 83, 0, 0, 0, 100, 100, 85, 0, 9, 0, 0, 78),
	(146153, 8, 2016, 100, 82, 80, 0, 0, 0, 100, 100, 85, 0, 9, 0, 0, 78),
	(146153, 7, 2016, 100, 81, 80, 0, 0, 0, 100, 100, 85, 0, 9, 0, 0, 78),
	(146153, 6, 2016, 96, 70, 69, 0, 0, 0, 100, 100, 85, 0, 9, 0, 0, 78),
	(146153, 5, 2016, 100, 51, 51, 0, 0, 0, 100, 100, 85, 0, 9, 0, 0, 78),
	(136460, 9, 0, 100, 78, 73, 0, 0, 0, 100, 100, 100, 0, 0, 0, 0, 78),
	(136460, 8, 0, 100, 78, 73, 0, 0, 0, 100, 100, 100, 0, 0, 0, 0, 78),
	(136460, 7, 0, 100, 78, 73, 0, 0, 0, 100, 100, 100, 0, 0, 0, 0, 78),
	(136460, 6, 0, 100, 78, 73, 0, 0, 0, 100, 100, 100, 0, 0, 0, 0, 78),
	(136460, 5, 0, 100, 78, 73, 0, 0, 0, 100, 100, 100, 0, 0, 0, 0, 78);
/*!40000 ALTER TABLE `metrics` ENABLE KEYS */;


-- Dumping structure for table test.metric_names
CREATE TABLE IF NOT EXISTS `metric_names` (
  `metric_name` varchar(75) NOT NULL DEFAULT 'NA',
  PRIMARY KEY (`metric_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table test.metric_names: ~14 rows (approximately)
/*!40000 ALTER TABLE `metric_names` DISABLE KEYS */;
REPLACE INTO `metric_names` (`metric_name`) VALUES
	('Attrition Rate'),
	('Billable Utilization'),
	('Effective Utilization'),
	('Effective Utilization - Forecast EOQ'),
	('Escalations'),
	('GSAP plan'),
	('InterDivision Movement'),
	('Less than 50% Utilized'),
	('Onsite Short and Long term movement'),
	('QNBR complaince'),
	('Resume Upload'),
	('SABA complaince'),
	('Time card complaince'),
	('USD project rejects');
/*!40000 ALTER TABLE `metric_names` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
