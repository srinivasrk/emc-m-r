-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.24 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for test
CREATE DATABASE IF NOT EXISTS `test` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `test`;


-- Dumping structure for table test.employee
CREATE TABLE IF NOT EXISTS `employee` (
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `emp_desg` varchar(50) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `last_login` date DEFAULT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table test.employee: ~2 rows (approximately)
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
REPLACE INTO `employee` (`emp_id`, `emp_name`, `emp_desg`, `password`, `last_login`) VALUES
	(1, 'abc', 'q', '123', '2016-08-31'),
	(2, 'xyz', 'q', '123', '2016-08-31');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;


-- Dumping structure for table test.mapping
CREATE TABLE IF NOT EXISTS `mapping` (
  `empid` int(11) NOT NULL,
  `mgrid` int(11) DEFAULT NULL,
  KEY `FK_mapping_employee` (`empid`),
  KEY `FK_mapping_employee_2` (`mgrid`),
  CONSTRAINT `FK_mapping_employee` FOREIGN KEY (`empid`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_mapping_employee_2` FOREIGN KEY (`mgrid`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table test.mapping: ~0 rows (approximately)
/*!40000 ALTER TABLE `mapping` DISABLE KEYS */;
REPLACE INTO `mapping` (`empid`, `mgrid`) VALUES
	(1, 2);
/*!40000 ALTER TABLE `mapping` ENABLE KEYS */;


-- Dumping structure for table test.metrics
CREATE TABLE IF NOT EXISTS `metrics` (
  `emp_id` int(11) DEFAULT NULL,
  `week_no` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `tc_complaince` int(11) NOT NULL,
  `eff_util` int(11) NOT NULL,
  `bill_util` int(11) NOT NULL,
  `less_50` int(11) NOT NULL,
  `onsite_sl` int(11) NOT NULL,
  `usd_rejects` int(11) NOT NULL,
  `qnbr_complaince` int(11) NOT NULL,
  `saba_compliant` int(11) NOT NULL,
  `resume_upload` int(11) NOT NULL,
  `escalations` int(11) NOT NULL,
  `gsap_plan` int(11) NOT NULL,
  `attrition_rate` int(11) NOT NULL,
  `inter_div_mov` int(11) NOT NULL,
  `effective_utilisation_eoq` int(11) NOT NULL,
  KEY `metrics_ibfk_1` (`emp_id`),
  CONSTRAINT `metrics_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table test.metrics: ~7 rows (approximately)
/*!40000 ALTER TABLE `metrics` DISABLE KEYS */;
REPLACE INTO `metrics` (`emp_id`, `week_no`, `year`, `tc_complaince`, `eff_util`, `bill_util`, `less_50`, `onsite_sl`, `usd_rejects`, `qnbr_complaince`, `saba_compliant`, `resume_upload`, `escalations`, `gsap_plan`, `attrition_rate`, `inter_div_mov`, `effective_utilisation_eoq`) VALUES
	(2, 2, 2016, 95, 89, 76, 56, 89, 78, 65, 78, 99, 100, 98, 100, 89, 78),
	(2, 3, 2016, 95, 89, 76, 56, 89, 78, 65, 78, 99, 100, 98, 100, 89, 78),
	(2, 5, 2016, 95, 89, 76, 56, 89, 78, 65, 78, 99, 100, 98, 100, 89, 78),
	(2, 1, 2016, 95, 89, 76, 56, 89, 78, 65, 78, 99, 100, 98, 100, 89, 78),
	(1, 2, 2016, 95, 89, 100, 56, 59, 90, 65, 78, 56, 100, 100, 89, 89, 100),
	(1, 4, 2016, 89, 100, 76, 100, 89, 78, 100, 100, 99, 90, 98, 100, 78, 78),
	(1, 1, 2016, 76, 89, 76, 14, 100, 100, 89, 87, 90, 89, 90, 67, 100, 90);
/*!40000 ALTER TABLE `metrics` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
