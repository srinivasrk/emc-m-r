<!DOCTYPE html>
<html>
<body>

<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test";
$timecard="";
$timec="";
    
$effectiveu="";
$effective="";

$billableu="";
$bill="";

$less50="";
$less="";

$onsitesl="";
$onsite="";
    
$usdrejects="";
$usdrej="";
    
$qnbrcom="";
$qnbr="";

$sabacom="";
$saba="";

$resumecom="";
$resume="";
    
$escalations="";
$escal="";

$gsapplan="";
$gsap="";
    
$attritionrate="";
$attrition="";
    
$interdev="";
$inter="";

$effectiveutil = "";
$effective2="";
    
    
// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);
// Check connection
if ($conn->connect_error) {
     die("Connection failed: " . $conn->connect_error);
} 

$sql = "select * from metrics  where emp_id in (select empid from mapping where mgrid ='98825') and week_no >1 and week_no order by week_no";
$result = $conn->query($sql);

if($result->num_rows>0)
{
    while($row = $result->fetch_assoc()) 
    {
        $timecard[''] = $row["Time_Card_Complaince"];
        $timec+=$timecard[''];
        
        $effectiveu[''] = $row["Effective_Utilization"];
        $effective+=$effectiveu[''];
        
        $billableu[''] = $row["Billable_Utilization"];
        $bill+=$billableu[''];
        
        $less50[''] = $row["less_50"];
        $less+=$less50[''];
        
        $onsitesl[''] = $row["onsite_sl"];
        $onsite+=$onsitesl[''];
        
        $usdrejects[''] = $row["usd_rejects"];
        $usdrej+=$usdrejects[''];
        
        $qnbrcom[''] = $row["qnbr_complaince"];
        $qnbr+=$qnbrcom[''];
        
        $sabacom[''] = $row["saba_compliant"];
        $saba+=$sabacom[''];
        
        $resumecom[''] = $row["resume_upload"];
        $resume+=$resumecom[''];
        
        $escalations[''] = $row["escalations"];
        $escal+=$escalations[''];
        
        $gsapplan[''] = $row["gsap_plan"];
        $gsap+=$gsapplan[''];
        
        $attritionrate[''] = $row["attrition_rate"];
        $attrition+=$attritionrate[''];
        
        $interdev[''] = $row["inter_div_mov"];
        $inter+=$interdev[''];
        
        $effectiveutil[''] = $row["effective_utilisation_eoq"];
        $effective2+=$effectiveutil[''];
    }
}

echo "Average time card:- " , $timec/4,"<br>";
echo "Average Effective utilization card:- " , $effective/4,"<br>";
echo "Average Billable utilization card:- " , $bill/4,"<br>";
echo "less than 50% utilization card:- " , $onsite/4,"<br>";
echo "USD rejects:- " , $usdrej/4,"<br>";
echo "qnbr compliance:- " , $qnbr/4,"<br>";
echo "resume upload:- " , $resume/4,"<br>";
echo "escalations:- " , $escal/4,"<br>";
echo "gsap plan:- " , $gsap/4,"<br>";
echo "attrition rate;- " , $attrition/4,"<br>";
echo "inter division movement:- " , $inter/4,"<br>";
echo "effective utilization end of quarter:- " , $effective2/4,"<br>";

    
    

$conn->close();
?>  

</body>
</html>