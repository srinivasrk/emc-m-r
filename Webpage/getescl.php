<?php 


// This is just an example of reading server side data and sending it to the client.
// It reads a json formatted text file and outputs it.

// Instead you can query your database and parse into JSON etc etc
$server= 'localhost';
$username = 'root';
$password ='';
$database = 'test';

$conn = mysqli_connect($server, $username, $password,$database);

if(!$conn){
    die("Connection Failed :" . mysqli_connect_error());
}



//$sql = "select week_no,less_50 from metrics where emp_id=1 order by week_no desc";
$sql = "select * from escl_tbl";


$result = mysqli_query($conn,$sql);

$htmldata ='';
$htmldata .= '<tr>';
$htmldata .= '<th style="font-weight:600;color:#6F05B7;padding:10px";>Esclation ID</th>';
$htmldata .= '<th style="padding:10px;color:#6F05B7;">Manager Name</th>';
$htmldata .= '<th style="padding:10px;color:#6F05B7;">Status</th>';
$htmldata .= '<th style="padding:10px;color:#6F05B7;">PM Involved</th>';
$htmldata .= '<th style="padding:10px;color:#6F05B7;">IS Involved</th>';
$htmldata .= '<th style="padding:10px;color:#6F05B7;">SA involved</th>';
$htmldata .= '<th style="padding:10px;color:#6F05B7;">Description</th>';
$htmldata .= '</tr>';
while($row = mysqli_fetch_row($result)){
    $htmldata .= '<tr>';
    if($row[2] == "Escalated") {
         for($x = 0 ;$x < 7 ; $x++){
                $htmldata .= '<td style="font-weight:600;color:black;" class="redclass">'.$row[$x].'</td>';
            }
    }
    }
   
    $htmldata .= "</tr>";


echo $htmldata;
?>