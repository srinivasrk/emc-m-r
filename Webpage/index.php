
<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test";
$timecard="";
$timec="";
    
$effectiveu="";
$effective="";

$billableu="";
$bill="";

$less50="";
$less="";

$onsitesl="";
$onsite="";
    
$usdrejects="";
$usdrej="";
    
$qnbrcom="";
$qnbr="";

$sabacom="";
$saba="";

$resumecom="";
$resume="";
    
$escalations="";
$escal="";

$gsapplan="";
$gsap="";
    
$attritionrate="";
$attrition="";
    
$interdev="";
$inter="";

$effectiveutil = "";
$effective2="";
    
    
// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);
// Check connection
if ($conn->connect_error) {
     die("Connection failed: " . $conn->connect_error);
} 

$sql = "select * from metrics  where emp_id in (select empid from mapping where mgrid ='136460') and week_no >0 and week_no < 52 order by week_no";
$result = $conn->query($sql);
$count = 0;
if($result->num_rows>0)
{
    while($row = $result->fetch_assoc()) 
    {
        $timecard[''] = $row["tc_complaince"];
        $timec+=$timecard[''];
        
        $effectiveu[''] = $row["eff_util"];
        $effective+=$effectiveu[''];
        
        $billableu[''] = $row["bill_util"];
        $bill+=$billableu[''];
        
        $less50[''] = $row["less_50"];
        $less+=$less50[''];
        
        $onsitesl[''] = $row["onsite_sl"];
        $onsite+=$onsitesl[''];
        
        $usdrejects[''] = $row["usd_rejects"];
        $usdrej+=$usdrejects[''];
        
        $qnbrcom[''] = $row["qnbr_complaince"];
        $qnbr+=$qnbrcom[''];
        
        $sabacom[''] = $row["saba_compliant"];
        $saba+=$sabacom[''];
        
        $resumecom[''] = $row["resume_upload"];
        $resume+=$resumecom[''];
        
        $escalations[''] = $row["escalations"];
        $escal+=$escalations[''];
        
        $gsapplan[''] = $row["gsap_plan"];
        $gsap+=$gsapplan[''];
        
        $attritionrate[''] = $row["attrition_rate"];
        $attrition+=$attritionrate[''];
        
        $interdev[''] = $row["inter_div_mov"];
        $inter+=$interdev[''];
        
        $effectiveutil[''] = $row["effective_utilisation_eoq"];
        $effective2+=$effectiveutil[''];
        $count = $count+1;
    }
}

$tc_val = round($timec/$count);
$eff_val = round($effective/$count);
$bill_val = round($bill/$count);
$less_50_val = round($less/$count);
$onsite_sl_val = round($onsite/$count);
$usd_rej_val = round($usdrej/$count);
$qnbr_val = round($qnbr/$count);
$resume_val = round($resume/$count);
$esc_val= round($escal/$count);
$gsap_val = round($gsap/$count);
$attr_val = round($attrition/$count);
$inter_val = round($inter/$count);
$eff_forecast_val = round($effective2/$count);
$saba_val = round($saba/$count);
    
    

$conn->close();
?> 


<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title> EMC M & R </title>

                <!-- Adding JQuery - Inevitabily :( -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
          <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/creative.js"> </script>
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/business-casual.css" rel="stylesheet">

         <!-- Plugin CSS -->
        <link rel="stylesheet" href="css/animate.min.css" type="text/css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/creative.css" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <!-- Fonts -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
       
         <script type="text/javascript">
             
              
              google.charts.load('current', {'packages':['gauge']});
              google.charts.setOnLoadCallback(drawChart);
              function drawChart() {
                
                var phpvar = "<?php echo $tc_val ?>";
                var x = parseInt(phpvar);
                
                var elem = document.getElementById("tc_comp_id");
                if(x <= 70)
                    {
                        elem.style.color = "red";
                    }
                  else if(x < 95)
                      {
                          elem.style.color = "orange";
                      }
                  else{
                      elem.style.color ="green";
                  }
                //
                       // elem.style.fontSize = "large";
                  
                var tc_data = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['', x]
                ]);
                  
                var phpvar = "<?php echo $eff_val ?>";
                var x = parseInt(phpvar);
                var elem = document.getElementById("eff_id");
                  
                   if(x <= 70)
                    {
                        elem.style.color = "red";
                    }
                  else if(x < 95)
                      {
                          elem.style.color = "orange";
                      }
                  else{
                      elem.style.color ="green";
                  }
                  
                  
                var eff_data = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['', x]
                ]);
                  
                var phpvar = "<?php echo $bill_val ?>";
                var x = parseInt(phpvar);
                   var elem = document.getElementById("bill_id");
                   if(x <= 70)
                    {
                        elem.style.color = "red";
                    }
                  else if(x < 95)
                      {
                          elem.style.color = "orange";
                      }
                  else{
                      elem.style.color ="green";
                  }
                  
                var bill_data = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['', x]
                ]);
                  
                var phpvar = "<?php echo $less_50_val ?>";
                var x = parseInt(phpvar);
                   var elem = document.getElementById("less_50_id");
                  if(x <= 70)
                    {
                        elem.style.color = "red";
                    }
                  else if(x < 95)
                      {
                          elem.style.color = "orange";
                      }
                  else{
                      elem.style.color ="green";
                  }
                   var less_50_data = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['', x]
                ]);
                  
                var phpvar = "<?php echo $onsite_sl_val ?>";
                var x = parseInt(phpvar);
                   var elem = document.getElementById("onsite_id");
                   if(x <= 70)
                    {
                        elem.style.color = "red";
                    }
                  else if(x < 95)
                      {
                          elem.style.color = "orange";
                      }
                  else{
                      elem.style.color ="green";
                  }
                  
                var onsite_data = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['', x]
                ]);
                  
                     var phpvar = "<?php echo $usd_rej_val ?>";
                var x = parseInt(phpvar);
                  var elem = document.getElementById("usd_id");
                   if(x <= 70)
                    {
                        elem.style.color = "red";
                    }
                  else if(x < 95)
                      {
                          elem.style.color = "orange";
                      }
                  else{
                      elem.style.color ="green";
                  }
                  
                  
                   var usd_rej_data = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['', x]
                ]);
                  
                     var phpvar = "<?php echo $qnbr_val ?>";
                var x = parseInt(phpvar);
                  var elem = document.getElementById("qnbr_id");
                   if(x <= 70)
                    {
                        elem.style.color = "red";
                    }
                  else if(x < 95)
                      {
                          elem.style.color = "orange";
                      }
                  else{
                      elem.style.color ="green";
                  }
                  
                  
                   var qnbr_data = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['', x]
                ]);
                  
                     var phpvar = "<?php echo $resume_val ?>";
                var x = parseInt(phpvar);
                  var elem = document.getElementById("resume_id");
                   if(x <= 70)
                    {
                        elem.style.color = "red";
                    }
                  else if(x < 95)
                      {
                          elem.style.color = "orange";
                      }
                  else{
                      elem.style.color ="green";
                  }
                  
                   var resume_data = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['', x]
                ]);
                  
                     var phpvar = "<?php echo $esc_val ?>";
                var x = parseInt(phpvar);
                  var elem = document.getElementById("esc_id");
                   if(x <= 70)
                    {
                        elem.style.color = "red";
                    }
                  else if(x < 95)
                      {
                          elem.style.color = "orange";
                      }
                  else{
                      elem.style.color ="green";
                  }
                  
                  
                   var esc_data = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['', x]
                ]);
                  
                     var phpvar = "<?php echo $gsap_val ?>";
                var x = parseInt(phpvar);
                  var elem = document.getElementById("gsap_id");
                   if(x <= 70)
                    {
                        elem.style.color = "red";
                    }
                  else if(x < 95)
                      {
                          elem.style.color = "orange";
                      }
                  else{
                      elem.style.color ="green";
                  }
                  
                   var gsap_data = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['', x]
                ]);
                  
                     var phpvar = "<?php echo $inter_val ?>";
                var x = parseInt(phpvar);
                  var elem = document.getElementById("sl_term_id");
                   if(x <= 70)
                    {
                        elem.style.color = "red";
                    }
                  else if(x < 95)
                      {
                          elem.style.color = "orange";
                      }
                  else{
                      elem.style.color ="green";
                  }
                  
                   var inter_data = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['', x]
                ]);
                  
                     var phpvar = "<?php echo $eff_forecast_val ?>";
                var x = parseInt(phpvar);
                   var elem = document.getElementById("forecast_eff_id")
                  
                   if(x <= 70)
                    {
                        elem.style.color = "red";
                    }
                  else if(x < 95)
                      {
                          elem.style.color = "orange";
                      }
                  else{
                      elem.style.color ="green";
                  }
                  
                   var eff_forecast_data = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['', x]
                ]);
                  
                var phpvar = "<?php echo $attr_val ?>";
                var x = parseInt(phpvar);
                  var elem = document.getElementById("attr_id")
                  
                   if(x <= 70)
                    {
                        elem.style.color = "red";
                    }
                  else if(x < 95)
                      {
                          elem.style.color = "orange";
                      }
                  else{
                      elem.style.color ="green";
                  }
                  
                   var attr_data = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['', x]
                ]);
                  
                var phpvar = "<?php echo $saba_val ?>";
                var x = parseInt(phpvar);
                  var elem = document.getElementById("saba_id")
                   if(x <= 70)
                    {
                        elem.style.color = "red";
                    }
                  else if(x < 95)
                      {
                          elem.style.color = "orange";
                      }
                  else{
                      elem.style.color ="green";
                  }
                  
                   var saba_data = google.visualization.arrayToDataTable([
                  ['Label', 'Value'],
                  ['', x]
                ]);
                  
                var options = {
                  width: 400, height: 120,
                  redFrom: 0, redTo: 70,
                  yellowFrom:70, yellowTo: 95,
                  greenFrom: 95,
                  greenTo: 100,
                  greenColor: "#00ff00"
                };

                
                var chart = new google.visualization.Gauge(document.getElementById('tc_chart'));

                chart.draw(tc_data, options);
                
                   
                  
                var chart1 = new google.visualization.Gauge(document.getElementById('eff_char1'));

                chart1.draw(eff_data, options);
                
                 
                  
                var chart2 = new google.visualization.Gauge(document.getElementById('bill_chart'));

                chart2.draw(bill_data, options);
                  
                  var chart3 = new google.visualization.Gauge(document.getElementById('less_50_chart'));

                chart3.draw(less_50_data, options);
                  
                  var chart4 = new google.visualization.Gauge(document.getElementById('onsite_chart'));

                chart4.draw(onsite_data, options);
                  
                  var chart5 = new google.visualization.Gauge(document.getElementById('usd_char'));

                chart5.draw(usd_rej_data, options);
                  
                  var chart6 = new google.visualization.Gauge(document.getElementById('qnbr_chart'));

                chart6.draw(qnbr_data, options);
                  
                  var chart7 = new google.visualization.Gauge(document.getElementById('saba_chart'));

                chart7.draw(saba_data, options);
                  
                  var chart8 = new google.visualization.Gauge(document.getElementById('resume_chart'));

                chart8.draw(resume_data, options);
                  
                  var chart9 = new google.visualization.Gauge(document.getElementById('escalations_chart'));

                chart9.draw(esc_data, options);
                  
                  var chart10 = new google.visualization.Gauge(document.getElementById('gsap_chart'));

                chart10.draw(gsap_data, options);
                  
                  var chart11 = new google.visualization.Gauge(document.getElementById('attr_chart'));

                chart11.draw(attr_data, options);
                  
                  var chart12 = new google.visualization.Gauge(document.getElementById('sl_inter_chart'));

                chart12.draw(inter_data, options);
                  
                  var chart13 = new google.visualization.Gauge(document.getElementById('forecast_chart'));

                chart13.draw(eff_forecast_data, options);
              }
             
           
    </script>
   
    </head>
    <body>
        <div class="header">
            <img style="width:100%;" src ="img/EMC_proven_professional_top_banner.jpg" alt="EMC Logo"/>
        </div>
        <div id="nav">
            <ul style="list-style-type:none">
                <li class="list-nav-item" style="margin-top : 20px;padding-right: 20px;"><a href="index.php"><img align="center" style="width:100%;height:120px;" src="img/dashboard-icon.png" /> </a></li>
                <li class="list-nav-item" style="margin-top : 40px;padding-right : 20px"><a href="index-metric.php"><img style="width:100%;height:100px;" src="img/metrics.png" /></a> </li>
                <li class="list-nav-item" style="margin-top : 40px;padding-right : 20px"><a href="index-manager.php"><img style="width:110%;height:120px;" src="img/employee-icon.png" /></a> </li>
                  <li class="list-nav-item" style="margin-top : 40px;padding-right : 20px"><a href="escalation-mgr.php"><img style="width:110%;height:120px;" src="img/escalation.jpg" /></a> </li>
            </ul>
        </div>
        <div id="content" >
            <div class="container">
                <div class="BodyBanner">
                    <p class="BodyBannerText"> Dashboard</p>
                </div>
                <hr />
                
               <p id="qtr_val" align="left" style="font-size:14px;font-weight:600;color:gray"> </p>
            

                  <table id="overall_week_view" class="container table table-bordered" style="padding:20px;margin:20px;">
                      
                    </table>
                
                <div class="row" style="margin:70px;margin-top:70px;">
                    <div class="gauge-borders col-lg-5" >
                            <p id="tc_comp_id" class="BodyText" align="center" style=""> Time Card Complaince</p>
                            <p> <div align="center" id="tc_chart" style="width: 100%; height: 120px;"> </div> </p>
                    </div>
                     <div class="gauge-borders col-lg-5">
                            <p id="eff_id" class="BodyText" align="center"> Effective Utilization</p>
                            <p> <div align="center" id="eff_char1" style="width: 100%; height: 120px;"></div> </p>
                    </div>
                    <div class="gauge-borders col-lg-5">
                            <p id="bill_id" class="BodyText" align="center"> Billable Utilization Value </p>
                            <p> <div align="center" id="bill_chart" style="width: 100%; height: 120px;"></div> </p>
                    </div>
                    
                    <div class="gauge-borders col-lg-5">
                            <p id="less_50_id" class="BodyText" align="center"> Members less than 50% Utilization </p>
                            <p> <div align="center" id="less_50_chart" style="width: 100%; height: 120px;"></div> </p>
                    </div>
                    <div class="gauge-borders col-lg-5">
                            <p id="onsite_id" class="BodyText" align="center"> Onsite Projects  </p>
                            <p> <div align="center" id="onsite_chart" style="width: 100%; height: 120px;"></div> </p>
                    </div>
                    <div class="gauge-borders col-lg-5">
                            <p  id="usd_id" class="BodyText" align="center"> USD Rejections</p>
                            <p> <div align="center" id="usd_char" style="width: 100%; height: 120px;"></div> </p>
                    </div>
                    <div class="gauge-borders col-lg-5">
                            <p id="qnbr_id" class="BodyText" align="center"> QNBR complaince </p>
                            <p> <div align="center" id="qnbr_chart" style="width: 100%; height: 120px;"></div> </p>
                    </div>
                    <div class="gauge-borders col-lg-5">
                            <p id="saba_id" class="BodyText" align="center"> SABA Complaince </p>
                            <p> <div align="center" id="saba_chart" style="width: 100%; height: 120px;"></div> </p>
                    </div>
                    <div class="gauge-borders col-lg-5">
                            <p id="resume_id" class="BodyText" align="center"> Resume Upload </p>
                            <p> <div align="center" id="resume_chart" style="width: 100%; height: 120px;"></div> </p>
                    </div>
                    <div class="gauge-borders col-lg-5">
                            <p id="esc_id" class="BodyText" align="center"> Escalations </p>
                            <p> <div align="center" id="escalations_chart" style="width: 100%; height: 120px;"></div> </p>
                    </div>
                    <div class="gauge-borders col-lg-5">
                            <p id="gsap_id" class="BodyText" align="center"> GSAP PLAN </p>
                            <p> <div align="center" id="gsap_chart" style="width: 100%; height: 120px;"></div> </p>
                    </div>
                    <div class="gauge-borders col-lg-5">
                            <p id="attr_id" class="BodyText"  align="center"> Attrition Rate </p>
                            <p> <div align="center" id="attr_chart" style="width: 100%; height: 120px;"></div> </p>
                    </div>
                    <div class="gauge-borders col-lg-5">
                            <p id="sl_term_id" class="BodyText" align="center"> Short/Long term inter-division Movement </p>
                            <p> <div align="center" id="sl_inter_chart" style="width: 100%; height: 120px;"></div> </p>
                    </div>
                    <div class="gauge-borders col-lg-5">
                            <p id="forecast_eff_id" class="BodyText" align="center"> Forecast - Effective utilization End Of Qtr   </p>
                            <p> <div align="center" id="forecast_chart" style="width: 100%; height: 120px;"></div> </p>
                    </div>
                </div>

            </div>
<footer class="footer">
    <hr />
    <p align="center" class="BodyBannerText" style="font-size:14px;color:red;" > All Rights Reserved - EMC 2016</p>
</footer>
        </div>
    </body>
</html>