-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.24 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for test
CREATE DATABASE IF NOT EXISTS `test` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `test`;


-- Dumping structure for table test.actionplan
CREATE TABLE IF NOT EXISTS `actionplan` (
  `empid` int(11) DEFAULT NULL,
  `metric_name` varchar(75) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `ac_plan` varchar(500) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `additional_comments` varchar(500) DEFAULT NULL,
  KEY `FK_actionplan_employee` (`empid`),
  KEY `FK_actionplan_metric_names` (`metric_name`),
  CONSTRAINT `FK_actionplan_employee` FOREIGN KEY (`empid`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_actionplan_metric_names` FOREIGN KEY (`metric_name`) REFERENCES `metric_names` (`metric_name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table test.actionplan: ~2 rows (approximately)
/*!40000 ALTER TABLE `actionplan` DISABLE KEYS */;
REPLACE INTO `actionplan` (`empid`, `metric_name`, `value`, `ac_plan`, `date`, `additional_comments`) VALUES
	(1, 'Effective Utilization', 89, 'No plan', NULL, NULL),
	(3, 'QNBR complaince', 67, 'I cold', NULL, NULL);
/*!40000 ALTER TABLE `actionplan` ENABLE KEYS */;


-- Dumping structure for table test.employee
CREATE TABLE IF NOT EXISTS `employee` (
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `emp_desg` varchar(50) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `last_login` date DEFAULT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table test.employee: ~4 rows (approximately)
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
REPLACE INTO `employee` (`emp_id`, `emp_name`, `emp_desg`, `password`, `last_login`) VALUES
	(1, 'abc', 'q', '123', '2016-08-31'),
	(2, 'xyz', 'q', '123', '2016-08-31'),
	(3, 'Mr. Sunil', 'q', '123', '2016-09-04'),
	(4, 'Mr Anil', 'q', '123', '2016-09-04');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;


-- Dumping structure for table test.mapping
CREATE TABLE IF NOT EXISTS `mapping` (
  `empid` int(11) NOT NULL,
  `mgrid` int(11) DEFAULT NULL,
  KEY `FK_mapping_employee` (`empid`),
  KEY `FK_mapping_employee_2` (`mgrid`),
  CONSTRAINT `FK_mapping_employee` FOREIGN KEY (`empid`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_mapping_employee_2` FOREIGN KEY (`mgrid`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table test.mapping: ~3 rows (approximately)
/*!40000 ALTER TABLE `mapping` DISABLE KEYS */;
REPLACE INTO `mapping` (`empid`, `mgrid`) VALUES
	(1, 2),
	(3, 2),
	(4, 2);
/*!40000 ALTER TABLE `mapping` ENABLE KEYS */;


-- Dumping structure for table test.metrics
CREATE TABLE IF NOT EXISTS `metrics` (
  `emp_id` int(11) DEFAULT NULL,
  `week_no` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `tc_complaince` int(11) NOT NULL,
  `eff_util` int(11) NOT NULL,
  `bill_util` int(11) NOT NULL,
  `less_50` int(11) NOT NULL,
  `onsite_sl` int(11) NOT NULL,
  `usd_rejects` int(11) NOT NULL,
  `qnbr_complaince` int(11) NOT NULL,
  `saba_compliant` int(11) NOT NULL,
  `resume_upload` int(11) NOT NULL,
  `escalations` int(11) NOT NULL,
  `gsap_plan` int(11) NOT NULL,
  `attrition_rate` int(11) NOT NULL,
  `inter_div_mov` int(11) NOT NULL,
  `effective_utilisation_eoq` int(11) NOT NULL,
  KEY `metrics_ibfk_1` (`emp_id`),
  CONSTRAINT `metrics_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table test.metrics: ~10 rows (approximately)
/*!40000 ALTER TABLE `metrics` DISABLE KEYS */;
REPLACE INTO `metrics` (`emp_id`, `week_no`, `year`, `tc_complaince`, `eff_util`, `bill_util`, `less_50`, `onsite_sl`, `usd_rejects`, `qnbr_complaince`, `saba_compliant`, `resume_upload`, `escalations`, `gsap_plan`, `attrition_rate`, `inter_div_mov`, `effective_utilisation_eoq`) VALUES
	(1, 33, 2016, 95, 14, 100, 56, 59, 90, 65, 78, 56, 100, 100, 89, 89, 100),
	(1, 34, 2016, 89, 100, 76, 100, 89, 78, 100, 100, 99, 90, 98, 100, 78, 78),
	(1, 35, 2016, 76, 89, 76, 14, 100, 100, 89, 87, 90, 89, 90, 67, 100, 90),
	(3, 33, 2016, 95, 89, 76, 56, 89, 78, 65, 78, 99, 100, 98, 100, 89, 78),
	(4, 35, 2016, 95, 89, 76, 56, 89, 78, 65, 78, 99, 100, 98, 100, 89, 78),
	(4, 34, 2016, 67, 99, 31, 80, 100, 100, 40, 71, 80, 50, 100, 67, 100, 78),
	(3, 34, 2016, 78, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
	(3, 35, 2016, 67, 99, 31, 80, 100, 100, 40, 71, 80, 50, 100, 67, 100, 78);
/*!40000 ALTER TABLE `metrics` ENABLE KEYS */;


-- Dumping structure for table test.metric_names
CREATE TABLE IF NOT EXISTS `metric_names` (
  `metric_name` varchar(75) NOT NULL DEFAULT 'NA',
  PRIMARY KEY (`metric_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table test.metric_names: ~14 rows (approximately)
/*!40000 ALTER TABLE `metric_names` DISABLE KEYS */;
REPLACE INTO `metric_names` (`metric_name`) VALUES
	('Attrition Rate'),
	('Billable Utilization'),
	('Effective Utilization'),
	('Effective Utilization - Forecast EOQ'),
	('Escalations'),
	('GSAP plan'),
	('InterDivision Movement'),
	('Less than 50% Utilized'),
	('Onsite Short and Long term movement'),
	('QNBR complaince'),
	('Resume Upload'),
	('SABA complaince'),
	('Time card complaince'),
	('USD project rejects');
/*!40000 ALTER TABLE `metric_names` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
