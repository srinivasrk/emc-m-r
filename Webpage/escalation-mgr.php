<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title> EMC M & R </title>

                <!-- Adding JQuery - Inevitabily :( -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
          <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/creative.js"> </script>
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/business-casual.css" rel="stylesheet">

         <!-- Plugin CSS -->
        <link rel="stylesheet" href="css/animate.min.css" type="text/css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/creative.css" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <!-- Fonts -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
        
         </head>
    <body>
        <div class="header">
            <img style="width:100%;" src ="img/EMC_proven_professional_top_banner.jpg" alt="EMC Logo"/>
        </div>
        <div id="nav">
            <ul style="list-style-type:none">
                <li class="list-nav-item" style="margin-top : 20px;padding-right: 20px;"><a href="index.php"><img align="center" style="width:100%;height:120px;" src="img/dashboard-icon.png" /> </a></li>
                <li class="list-nav-item" style="margin-top : 40px;padding-right : 20px"><a href="index-metric.php"><img style="width:100%;height:100px;" src="img/metrics.png" /></a> </li>
                <li class="list-nav-item" style="margin-top : 40px;padding-right : 20px"><a href="index-manager.php"><img style="width:110%;height:120px;" src="img/employee-icon.png" /></a> </li>
                 <li class="list-nav-item" style="margin-top : 40px;padding-right : 20px"><a href="escalation-mgr.php"><img style="width:110%;height:120px;" src="img/escalation.jpg" /></a> </li>
            </ul>
        </div>
        <div id="content" >
            <div class="container">
                <div class="BodyBanner">
                    <p class="BodyBannerText"> Dashboard</p>
                </div>
                <hr />
                <ul style="list-style-type:none;">
                    <li class="" style=""><a style="border : none" href=""><img style="float:left;width:360px;height:120px;" src="img/add_escl.png" /> </a></li>
                    <li class="" style=""><a style="border : none" href=""><img style="float:left;width:300px;height:120px;" src="img/edit_escl.png" /> </a></li>
                </ul>
                <table id="escl_data" class="container table table-bordered" style="padding:20px;margin:20px;">
               
                </table>
                
            </div>
<footer class="footer">
    <hr />
    <p align="center" class="BodyBannerText" style="font-size:14px;color:red;" > All Rights Reserved - EMC 2016</p>
</footer>
        </div>
    </body>
</html>