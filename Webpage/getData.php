<?php 

// This is just an example of reading server side data and sending it to the client.
// It reads a json formatted text file and outputs it.
if(isset($_POST['param1']) && isset($_POST['param2']))
{
    $param1 = $_POST['param1'];
    $mgrname = $_POST['param2'];

    // Do whatever you want with the $uid
}
// Instead you can query your database and parse into JSON etc etc
$server= 'localhost';
$username = 'root';
$password ='';
$database = 'test';

$conn = mysqli_connect($server, $username, $password,$database);

if(!$conn){
    die("Connection Failed :" . mysqli_connect_error());
}
$array['cols'][] = array('type' => 'string','label' => 'Week ');
$params = explode("|",$param1);

//$sql = "select week_no,less_50 from metrics where emp_id=1 order by week_no desc";
$sql = "select week_no,";
for($x = 0 ; $x < count($params); $x++)
{
  
    switch (trim($params[$x])) {
    case "tc_complaince":
            $sql = $sql." tc_complaince,";
            $array['cols'][] = array('type' => 'number','label' => 'Time Card Complaince');
            break;
    case "eff_util":
            $sql = $sql." eff_util,";
            $array['cols'][] = array('type' => 'number','label' => 'Effective Utiliztion');
            break;
    case "bill_util":
            $sql = $sql." bill_util,";
            $array['cols'][] = array('type' => 'number','label' => 'Billable Utiliztion');
            break;
    case "less_50":
            $sql = $sql." less_50,";
            $array['cols'][] = array('type' => 'number','label' => 'Less than 50% Utiliztion');
            break;
    case "onsite_sl":
            $sql = $sql." onsite_sl,";
            $array['cols'][] = array('type' => 'number','label' => 'Onsite project short/long term');
            break;
    case "usd_rejects":
            $sql = $sql." usd_rejects,";
            $array['cols'][] = array('type' => 'number','label' => 'USD Rejections');
            break;
     case "qnbr_complaince":
            $sql = $sql." qnbr_complaince,";
            $array['cols'][] = array('type' => 'number','label' => 'QNBR Complaince');
            break;
     case "saba_compliant":
            $sql = $sql." saba_compliant,";
            $array['cols'][] = array('type' => 'number','label' => 'SABA Complaince');
            break;
     case "resume_upload":
            $sql = $sql." resume_upload,";
            $array['cols'][] = array('type' => 'number','label' => 'Resume Upload');
            break;
     case "escalations":
            $sql = $sql." escalations,";
            $array['cols'][] = array('type' => 'number','label' => 'Escalations');
            break;
     case "gsap_plan":
            $sql = $sql." gsap_plan,";
            $array['cols'][] = array('type' => 'number','label' => 'GSAP Plan');
            break;
     case "attrition_rate":
            $sql = $sql." attrition_rate,";
            $array['cols'][] = array('type' => 'number','label' => 'Attrition Rate');
            break;
     case "inter_div_mov":
            $sql = $sql." inter_div_mov,";
            $array['cols'][] = array('type' => 'number','label' => 'Inter-Division Movement');
            break;
     case "effective_utilisation_eoq":
            $sql = $sql." effective_utilisation_eoq";
            $array['cols'][] = array('type' => 'number','label' => 'Effective Utilization End of Quarter - Forecast');
            break;
            
    default:
        
}
}

$sql = rtrim($sql, ",");

$sql = $sql." from employee e join metrics m on e.emp_id = m.emp_id  where emp_name ='".$mgrname."' order by week_no desc";


$result = mysqli_query($conn, $sql);

  
 while ($row=mysqli_fetch_row($result))
{
     $overall=array();
     $temp = array('v' => 'Week '. $row[0]);
     
     array_push($overall,$temp);
     
     for($x=1;$x<count($row);$x++)
     {
         $temp1 = array('v' => $row[$x]);
         array_push($overall,$temp1);
     }
    
   // $temp = array('v'=> 'Week '. $row["week_no"]);
    $array['rows'][]['c'] = $overall;
    //$array['rows'][]['c'] = array(array('v' => 'Week '. $row["week_no"]),array('v' => $row["tc_complaince"]));
}


    
echo json_encode($array);


?>